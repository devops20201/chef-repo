#
# Cookbook:: custom_tomcat
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.

package 'java-1.8.0-openjdk.x86_64' do
action :install
end

user node['tomcat']['user'] do
   comment 'tomcat user'
   home node['tomcat']['homedir']
 
  
  system true
  shell '/bin/nologin'
  
end

group node['tomcat']['group'] do
  action :create
end

package 'wget' do
  action :install
end








